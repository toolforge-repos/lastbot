# LastBot

Lexique :

AC -> age change

dp -> module 'dateutil.parser'

dtc -> module 'detectorsLB'

EXC -> exception

I -> index

INS -> insult

IS -> ISO-8601

LB -> LastBot

lxq -> module 'lexiqueLB'

OOC -> out of context

pgcl -> module 'pagecleanersLB'

POOC -> potentially out of context

PRC -> processing

RC -> recent change

scr -> module 'scoreLB'

tmcc -> module 'timecalcLB'

UNEP -> UNIX Epoch

WEX -> words and expressions

WBF -> word ban file
